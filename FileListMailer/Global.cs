﻿using Microsoft.Extensions.Logging;
using FileListMailer.Config;
using System;
using System.Collections.Generic;
using System.Text;

namespace FileListMailer
{
    public static class Global
    {


#if DEBUG
        public static bool Debug = true;
#else
        public static bool Debug = false;
#endif

        public static string SettingsRegistryKeyPath = "SOFTWARE\\PortUp Gmbh\\FileListMailer";

        public static string ConfigFilePath;
        
        internal static BasicConfig BasicConfig;

        // internal static ILoggerFactory loggerFactory;
    }
}
