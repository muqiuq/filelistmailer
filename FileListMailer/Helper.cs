﻿using FileListMailer.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.Json;

namespace FileListMailer
{
    public static class Helper
    {


        public static void SendMail(string to, string subject, string body, string fromName = null)
        {
            var c = new SmtpClient(Global.BasicConfig.SmtpHost, Global.BasicConfig.SmtpPort);
            c.EnableSsl = true;
            c.Credentials = new NetworkCredential(Global.BasicConfig.SmtpUsername, Global.BasicConfig.SmtpPassword);
            var mailMessage = new MailMessage(Global.BasicConfig.SmtpFrom, to)
            {
                IsBodyHtml = true,
                Subject = subject,
                Body = body
            };
            if (fromName != null) mailMessage.From = new MailAddress(Global.BasicConfig.SmtpFrom, fromName);
            c.Send(mailMessage);
        }

        public static void CreateExampleConfigFile(string configFilePath)
        {
            var exampleConfig = new BasicConfig();
            exampleConfig.Mappings.Add(new FolderToEmail(false, "FolderName", "Max Müller", "Lieber", "Subject", "Message {0}", "asd@asd.ch", new List<string>()));

            var json = JsonSerializer.Serialize(exampleConfig, new JsonSerializerOptions() { WriteIndented = true });

            File.WriteAllText(configFilePath, json, Encoding.UTF8);
        }

    }
}
