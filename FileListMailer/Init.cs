﻿using Microsoft.Extensions.Logging;
using Microsoft.Win32;
using FileListMailer.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace FileListMailer
{
    public static class Init
    {
        private static Logger logger;

        public static void InitLogger()
        {
            logger = new Logger("FileListMailer.Init");
            logger.Debug("Logger OK");
        }

        public static void LoadRegistryConfig()
        {
            var rootRegistryKey = Registry.LocalMachine;
            if (Global.Debug)
            {
                rootRegistryKey = Registry.CurrentUser;
            }

            var registrySettingsKey = rootRegistryKey.OpenSubKey(Global.SettingsRegistryKeyPath, true);
            if (registrySettingsKey == null)
            {
                registrySettingsKey = rootRegistryKey.CreateSubKey(Global.SettingsRegistryKeyPath, true);
            }
            var registrySettingConfigFilePath = registrySettingsKey.GetValue("Path");
            if (registrySettingConfigFilePath == null || !(registrySettingsKey.GetType() == typeof(string)))
            {
                registrySettingsKey.SetValue("Path", Path.Combine(Directory.GetCurrentDirectory(), "config.json"), RegistryValueKind.String);
                registrySettingConfigFilePath = registrySettingsKey.GetValue("Path");
            }
            
            Global.ConfigFilePath = (string) registrySettingConfigFilePath;

            logger.Debug("Registry loaded");
        }

        public static void LoadConfig(string configFilePath = null)
        {
            if (configFilePath != null) Global.ConfigFilePath = configFilePath;

            var jsonRaw = File.ReadAllText(Global.ConfigFilePath, Encoding.UTF8);

            Global.BasicConfig = JsonSerializer.Deserialize<BasicConfig>(jsonRaw);

            logger.Debug("Config loaded");
        }

    }
}
