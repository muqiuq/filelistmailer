﻿using Microsoft.Win32;
using System;
using System.Net;
using System.Net.Mail;
using CommandLine;
using FileListMailer.Cli;
using System.Linq;
using System.Threading;

namespace FileListMailer
{
    class Program
    {
        private static Logger logger;

        static void Main(string[] args)
        {
            if (args.Any(i => i.Trim() == "--debug")) Global.Debug = true;

            Init.InitLogger();

            logger = new Logger(typeof(Program));

            AppDomain.CurrentDomain.FirstChanceException += (sender, eventArgs) =>
            {
                logger.Error("FirstChanceException", eventArgs.Exception);
            };

            // Init.LoadRegistryConfig();
            
            var parsed = Parser.Default.ParseArguments<RunAction>(args);
            var result = parsed.MapResult(
                (RunAction ra) => ra.Execute(),
                e => 1);

        } 
    }
}
