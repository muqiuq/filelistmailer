﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileListMailer.Config
{
    class BasicConfig
    {

        public List<FolderToEmail> Mappings { get; set; } = new List<FolderToEmail>();

        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; } = 587;
        public bool SmtpUseEncryption { get; set; } = true;
        public string SmtpDefaultTo { get; set; }
        public string SmtpFrom { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        public string OverrideEMailReceiver { get; set; } = "";

        public Dictionary<string, string> Messages { get; set; } = new Dictionary<string, string>();
        public Dictionary<string, string> Subjects { get; set; } = new Dictionary<string, string>();
    }
}
