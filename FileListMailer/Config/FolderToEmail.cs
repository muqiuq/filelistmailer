﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileListMailer.Config
{
    public class FolderToEmail
    {
        public FolderToEmail(bool enabled, string path, string nickname, string salutation, string subject, string message, string eMail, List<string> filters)
        {
            Enabled = enabled;
            Path = path;
            Nickname = nickname;
            Salutation = salutation;
            Subject = subject;
            Message = message;
            EMail = eMail;
            Filters = filters;
        }

        public bool Enabled { get; set; }

        public string Path { get; set; }

        public string Nickname { get; set; }

        public string Salutation { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

        public string EMail { get; set; }

        public string FromDisplayName { get; set; }

        public List<string> Filters { get; set; } = new List<string>();

    }
}
