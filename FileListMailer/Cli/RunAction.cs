﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;


namespace FileListMailer.Cli
{
    [Verb("run", HelpText = "Run")]
    public class RunAction : BaseConfig
    {
        private Logger logger;

        [Option('c', "config", Required = true, HelpText = "config file (json) to execute")]
        public string Filename { get; set; }

        [Option("create", Required = false, HelpText = "create example config file", Default = false)]
        public bool CreateExampleConfigFile { get; set; }

        public RunAction()
        {
            logger = new Logger(typeof(RunAction));
        }

        protected override int Run()
        {
            if (!File.Exists(Filename))
            {
                if(CreateExampleConfigFile)
                {
                    Helper.CreateExampleConfigFile(Filename);
                    logger.Info($"Created example config file at {Filename}");
                    return 0;
                }
                else
                {
                    logger.Error($"Config file not found {Filename}");
                    return 1;
                }
            }else if(CreateExampleConfigFile)
            {
                logger.Error($"Config file already exists {Filename}");
                return -1;
            }
            Init.LoadConfig(Filename);
            foreach (var folderToEMail in Global.BasicConfig.Mappings)
            {
                if (!folderToEMail.Enabled) continue;

                CheckDirectoryTask cdt = new CheckDirectoryTask(folderToEMail, 
                    Global.BasicConfig.Messages, 
                    Global.BasicConfig.Subjects,
                    Global.BasicConfig.OverrideEMailReceiver
                    );

                cdt.Run();
            }

            return 0;
        }

    }
}
