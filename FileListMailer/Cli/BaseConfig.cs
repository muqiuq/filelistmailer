﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileListMailer.Cli
{
    public abstract class BaseConfig
    {

        [Option("debug", Required = false, HelpText = "debug", Default = false)]
        public bool Debug { get; set; }

        protected abstract int Run();

        public int Execute()
        {
            Global.Debug = Debug;
            return Run();
        }

    }
}
