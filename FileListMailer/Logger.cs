﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileListMailer
{
    public class Logger
    {
        private readonly string source;

        public Logger(string source)
        {
            this.source = source;
        }

        public Logger(Type source) : this(source.Name)
        {

        }

        public static void Log(string category, string source, string text, Exception e = null)
        {
            string fullText = text;

            if(e != null)
            {
                fullText += Environment.NewLine + e.ToString();
            }

            Console.WriteLine(string.Format("{0} {1} {2}: {3} > {4}", 
                DateTime.Now.ToShortDateString(),
                DateTime.Now.ToShortTimeString(),
                category,
                source,
                text
                ));
        }

        public void Error(string text, Exception e = null)
        {
            Log("error", source, text, e);
        }

        public void Info(string text)
        {
            Log("info", source, text);
        }

        public void Debug(string text)
        {
            if(Global.Debug)
                Log("debug", source, text);
        }

    }
}
