# FileListMailer

FileListMailer is a CLI tool to send an email with a list of files from a specified directory, for file tracking and reporting.

The tool was created to inform users about updates in a folder designated for incoming invoices requiring signatures.

## Usage

 - Create example configuration file
```powershell
.\FileListMailer.exe --create -c "config.json"
```
 - Edit `config.json` and add mappings
   - If `OverrideEMailReceiver` is set all e-mails will be send to this address. 
 - Execute configuration = Send e-mails according to configuration
```powershell
.\FileListMailer.exe -c "config.json"
```

